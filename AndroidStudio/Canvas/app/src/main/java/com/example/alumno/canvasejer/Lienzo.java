package com.example.alumno.canvasejer;

import android.content.*;
import android.graphics.*;
import android.view.View;

public class Lienzo extends View {
    Paint p;
    int x, y, x0, y0, d, rand, xaux, yaux;
    Punto v1, v2, v3, pt, pm, aux;

    public Lienzo(Context c) {
        super(c);
    }

    protected void onDraw(Canvas c) {
        super.onDraw(c); // Canvas pinta atributos
        p = new Paint(); // Paint asigna atributos
        x = c.getWidth();
        x0 = x / 2; // También: getMeasuredWidth(), o getRight(), x=480
        y = c.getHeight();
        y0 = y / 2; // También: getMeasuredHeight(), o getBottom(), y=762
        p.setColor(Color.WHITE); // Fondo blanco
        c.drawPaint(p);
        p.setColor(Color.BLUE); // Texto azul
        c.drawLine(0, y / 2, x, y / 2, p);
        c.drawLine(x / 2, 0, x / 2, y, p);

        p.setTextSize(35);
        p.setStrokeWidth(3);

        v1 = new Punto(x0, y0 - 250);
        c.drawPoint(v1.x, v1.y, p);
        c.drawText("(" + v1.x + "," + v1.y + ")", v1.x - 100, v1.y - 30, p);

        v2 = new Punto(x0 + 250, y0 + 250);
        c.drawPoint(v2.x, v2.y, p);
        c.drawText("(" + v2.x + "," + v2.y + ")", v2.x - 100, v2.y - 30, p);

        v3 = new Punto(x0 - 250, y0 + 250);
        c.drawPoint(v3.x, v3.y, p);
        c.drawText("(" + v3.x + "," + v3.y + ")", v3.x - 100, v3.y - 30, p);

        pt = puntoAleatorio(v1, v2, v3, x, y);
        p.setColor(Color.MAGENTA); // Texto azul
        c.drawPoint(pt.x, pt.y, p);
        c.drawText("(" + pt.x + "," + pt.y + ")", pt.x - 100, pt.y - 30, p);
        p.setColor(Color.RED);
        for (int i = 1; i <= 1000000; i++) {
            rand = (int) (Math.random() * 3 + 1);
            if (rand == 1)
                aux = v1;
            else if (rand == 2)
                aux = v2;
            else if (rand == 3)
                aux = v3;
            d = calculaDistancia(aux, pt);
            xaux = (aux.x + pt.x)/2;
            yaux = (aux.y + pt.y)/2;
            pm = new Punto(xaux, yaux);
            c.drawPoint(pm.x, pm.y, p);
            pt = pm;
        }
    }


    public int calculaDistancia(Punto v, Punto pt) {
        int aux = (int) Math.sqrt(Math.pow(pt.x - v.x, 2) + Math.pow(pt.y - v.y, 2));
        return aux;
    }

    public Punto puntoAleatorio(Punto v1, Punto v2, Punto v3, int x, int y) {
        Punto aux;
        do {
            aux = new Punto((int) (Math.random() * x), (int) (Math.random() * y));
        }
        while ((aux.y > v1.y - 50) && (aux.y < v2.y + 50) && (aux.x > v3.x - 20) && (aux.x < v2.x + 20));
        return aux;
    }
}
