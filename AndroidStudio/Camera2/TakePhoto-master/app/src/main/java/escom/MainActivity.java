package escom;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressLint("Registered")
public class MainActivity extends Activity {

    static final int REQUEST_IMAGE_CAPTURE =1;
    private final String ruta =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +
                    "/misfotos/";
    private File f = new File(ruta);
    private Button jbn;

    ImageView iv,clk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());


        super.onCreate(savedInstanceState);
        setContentView(escom.R.layout.activity_main);

        jbn = (Button) findViewById(escom.R.id.clk);

        jbn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = ruta + getCode() + ".jpg";
                File f1 = new File(s);
                try{
                    f1.createNewFile();
                }catch(IOException ex){
                    Log.e("Error", "Error:" + ex);
                }
                Uri u = Uri.fromFile(f1);
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                i.putExtra(MediaStore.EXTRA_OUTPUT, u);
                startActivityForResult(i,REQUEST_IMAGE_CAPTURE);
            }
        });


    }

    private String getCode() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyymmddhhmmss");
        String n = "pic_" + sdf.format(new Date());;
        return n;    }


}
