package com.example.alumno.patron;

import android.content.Context;
import android.graphics.*;
import android.view.*;
import android.widget.Toast;

class MiLienzo extends View {
    Context contexto;
    float   x=0, y=0, xInicial=0, yInicial=0;

    float px[] = new float[9];
    float py[] = new float[9];
    boolean already[] = new boolean[9];

    String patronDefinido = "0,1,3,";

    String patronEscrito = "";

    String  s="";

    Path pa=new Path();

    public MiLienzo(Context c){
        super(c);
        contexto = c;
    }
    @Override
    protected void onDraw(Canvas c){

        px[0] = getWidth()/4;
        py[0] = getHeight()/2;
        px[1] = px[0]*2;
        py[1] = py[0];
        px[2] = px[0]*3;
        py[2] = py[0];
        px[3] = px[0];
        py[3] = py[0]+px[0];
        px[4] = px[0]*2;
        py[4] = py[3];
        px[5] = px[0]*3;
        py[5] = py[3];
        px[6] = px[0];
        py[6] = py[0]+px[0]+px[0];
        px[7] = px[0]*2;
        py[7] = py[6];
        px[8] = px[0]*3;
        py[8] = py[6];

        c.drawColor(Color.BLACK); //Fondo negro
        Paint p = new Paint();
        if (s=="unpressed"){
            c.drawColor(Color.BLACK); //Fondo negro
            x=0;
            y=0;
            pa.reset();
            for (int j=0;j<already.length;j++){
                already[j] = false;
            }
            System.out.println(patronEscrito);
            if (patronEscrito.equals(patronDefinido)){
                Toast.makeText(contexto,"Patron correcto",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(contexto,"Patron incorrecto",Toast.LENGTH_LONG).show();
            }
            patronEscrito = "";
        }else{
            p.setColor(Color.rgb(215,215,215));
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(6);
            if (s=="moved"){
                c.drawPath(pa, p);
                if (isDentro(c,p)){
                    if (pasaPor(c,pa,p)){
                        System.out.println("Pasa por nuevo punto");
                    }
                }
            }
        }
        //Se dibujaran los circulos
        p.setColor(Color.rgb(215,215,215));
        p.setStyle(Paint.Style.FILL);
        c.drawCircle(px[0],py[0],20,p);
        c.drawCircle(px[1],py[1],20,p);
        c.drawCircle(px[2],py[2],20,p);
        c.drawCircle(px[3],py[3],20,p);
        c.drawCircle(px[4],py[4],20,p);
        c.drawCircle(px[5],py[5],20,p);
        c.drawCircle(px[6],py[6],20,p);
        c.drawCircle(px[7],py[7],20,p);
        c.drawCircle(px[8],py[8],20,p);
    }

    private boolean pasaPor(Canvas c, Path pa, Paint p) {
        for (int i = 0; i<px.length; i++){
            if (already[i]==false){
                if(Math.sqrt(Math.pow((px[i]-x),2)+Math.pow((py[i]-y),2)) < 20){
                    pa.moveTo(xInicial, yInicial);
                    pa.lineTo(px[i],py[i]);
                    c.drawPath(pa, p);
                    xInicial = px[i];
                    yInicial = py[i];
                    pa.moveTo(xInicial, yInicial);
                    already[i]=true;
                    if (!patronEscrito.contains(String.valueOf(i))){
                        patronEscrito+=i+",";
                    }
                    return true;
                }
            }
        }
        return false;
    }


    private boolean isDentro(Canvas c, Paint p) {
        for (int i = 0; i<px.length; i++){
            if(Math.sqrt(Math.pow((px[i]-xInicial),2)+Math.pow((py[i]-yInicial),2)) < 20){
                c.drawLine(px[i],py[i],x,y,p);
                already[i]=true;
                if (!patronEscrito.contains(String.valueOf(i))){
                    patronEscrito+=i+",";
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent me){
        if (x==0 && y==0){
            xInicial = me.getX();
            yInicial = me.getY();
        }
        x = me.getX();
        y = me.getY();
        if(me.getAction()==MotionEvent.ACTION_DOWN) { //Significa que esta presionado
            s = "pressed";
        }
        if(me.getAction()==MotionEvent.ACTION_MOVE) { //Significa que lo esta moviendo
            s = "moved";
        }
        if (me.getAction()==MotionEvent.ACTION_UP){ //Significa que lo solto
            s = "unpressed";
        }
        invalidate();
        return true;
    }
}