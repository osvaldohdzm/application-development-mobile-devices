package com.escom.proyecto;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashAct extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_splash);


        Intent mainIntent = new Intent(SplashAct.this, MainActivity.class);
        startActivity(mainIntent);


    }
}
