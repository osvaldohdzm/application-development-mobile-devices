package com.example.a1052941934.proyecto2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements SensorEventListener{
    TextView mReadBuffer;
    Handler bluetoothIn;
    String mensaje = "";
    SensorManager sensorManager;
    Sensor gyroscopeSensor;
    float [] history = new float[3];
    String [] direction = {"-","-","-"};
    String actual="";
    private double l,m=0;
    int AuxCentro = 0;
    int AuxArriba = 0;
    int AuxAbajo = 0;
    int AuxIzquierda = 0;
    int AuxDerecha = 0;

    final int handlerState = 0;        				 //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mReadBuffer = (TextView) findViewById(R.id.giro);
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        gyroscopeSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        //Link the buttons and textViews to respective views

        //sensorManager.registerListener(this,gyroscopeSensor,SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, gyroscopeSensor, SensorManager.SENSOR_DELAY_NORMAL);
        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();

    }



    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get MAC address from DeviceListActivity via intent
        Intent intent = getIntent();

        //Get the MAC address from the DeviceListActivty via EXTRA
        address = intent.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        //create device and set the MAC address
        //Log.i("ramiro", "adress : " + address);
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
        } catch (IOException e) {
            try
            {
                btSocket.close();
            } catch (IOException e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        //I send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
        mConnectedThread.write("x");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        StringBuilder builder = new StringBuilder();
        float xChange = history[0] - event.values[0];
        float yChange = history[1] - event.values[1];
        float zChange = history[2] - event.values[2];

        history[0] = event.values[0];
        history[1] = event.values[1];
        history[2] = event.values[2];
        //arriba abajo centro izquierda derecha
        if (xChange > 1.5f && !actual.equals("izq")){
            direction[0] = "Izquierda";
            System.out.println("Izquierda");
            actual="izq";
            if(mConnectedThread != null) //First check to make sure thread created
            {
                mConnectedThread.write("0,0,0,1,0");
            }
        }else if (xChange < -1.5f && !actual.equals("der")){
            direction[0] = "Derecha";
            System.out.println("Derecha");
            actual="der";
            if(mConnectedThread != null) //First check to make sure thread created
            {
                mConnectedThread.write("0,0,0,0,1");
            }
        }else if (yChange > 1.5f && !actual.equals("aba")){
            direction[1] = "Abajo";
            System.out.println("Abajo");
            actual="aba";
            if(mConnectedThread != null) //First check to make sure thread created
            {
                mConnectedThread.write("0,1,0,0,0");
            }
        }
        else if (yChange < -1.5f && !actual.equals("arr")){
            direction[1] = "Arriba";
            System.out.println("Arriba");
            actual="arr";
            if(mConnectedThread != null) //First check to make sure thread created
            {
                mConnectedThread.write("1,0,0,0,0");
            }
        }else if (zChange > 1.5f && !actual.equals("atr")){
            direction[2] = "Atras";
            System.out.println("Atras");
            actual="atr";
            if(mConnectedThread != null) //First check to make sure thread created
            {
                mConnectedThread.write("0,0,1,0,0");
            }
        }
        else if (zChange < -1.5f && !actual.equals("ade")){
            direction[2] = "Adelante";
            System.out.println("Adelante");
            actual = "ade";
            if(mConnectedThread != null) { //First check to make sure thread created
                mConnectedThread.write("0,0,0,0,0");
            }
        }
        builder.setLength(0);
        builder.append("x: ");
        builder.append(direction[0]);
        builder.append("\ny: ");
        builder.append(direction[1]);
        builder.append("\nZ: ");
        builder.append(direction[2]);
        mReadBuffer.setText(builder.toString());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    //create new class for connect thread
    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);        	//read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }
        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "La Conexión fallo", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}

