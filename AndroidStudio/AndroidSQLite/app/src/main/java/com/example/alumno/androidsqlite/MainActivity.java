package com.example.alumno.androidsqlite;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;

public class MainActivity extends Activity {
    EditText        jetI, jetN;
    Button          jbnA, jbnL, jbnD, jbnE, jbnC;
    TextView        jtvL;
    SQLiteDatabase  sqld;
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbms);
        jetI = (EditText)   findViewById(R.id.xetI);
        jetN = (EditText)   findViewById(R.id.xetN);
        jbnE = (Button)     findViewById(R.id.xbnE);
        jbnA = (Button)     findViewById(R.id.xbnA);
        jbnL = (Button)     findViewById(R.id.xbnL);
        //jbnC = (Button)     findViewById(R.id.xbnC);
        jbnD = (Button) findViewById(R.id.xbnD);
        jtvL = (TextView)   findViewById(R.id.xtvL);
        DbmsSQLiteHelper dsqlh = new DbmsSQLiteHelper(this, "DBContactos", null, 1);
        sqld = dsqlh.getWritableDatabase();


        jbnD.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String          id = jetI.getText().toString();
                sqld.execSQL("DELETE FROM Contactos WHERE id="+id);
            }
        });

/*
        jbnC.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String name = jetN.getText().toString();
                String id, nombre;
                Cursor c = sqld.rawQuery("SELECT id, nombre FROM Contactos WHERE nombre ='"+name+"''", null);
                jtvL.setText("");
                if (c.moveToFirst()) {
                    do {
                        id = c.getString(0);
                        nombre = c.getString(1);
                        jtvL.append(" " + id + "\t" + nombre + "\n");
                    } while(c.moveToNext());
                }
            }
        });*/

        jbnE.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String          id = jetI.getText().toString();
                String          name = jetN.getText().toString();
                sqld.execSQL("UPDATE Contactos SET nombre = '"+name+"' WHERE id ="+id);
            }
        });


        jbnA.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String          id = jetI.getText().toString();
                String          nombre = jetN.getText().toString();
                ContentValues   cv = new ContentValues();
                //cv.put("id", id);
                cv.put("nombre", nombre);
                sqld.insert("Contactos", null, cv);
                jetI.setText(""); jetN.setText("");
            }
        });

        jbnL.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                String id, nombre;
                Cursor c = sqld.rawQuery("SELECT id,nombre FROM Contactos", null);
                jtvL.setText("");
                if (c.moveToFirst()) {
                    do {
                        id = c.getString(0);
                        nombre = c.getString(1);
                        jtvL.append(" " + id + "\t" + nombre + "\n");
                    } while(c.moveToNext());
                }
            }
        });
    }
}
