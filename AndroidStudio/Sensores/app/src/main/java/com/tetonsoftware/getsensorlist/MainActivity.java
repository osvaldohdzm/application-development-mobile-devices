package com.tetonsoftware.getsensorlist;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

public class MainActivity extends ListActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		int[] unicode = new int[22];
		unicode[0] = 0x1F53D;
		unicode[1] = 0x1F501;
		unicode[2] = 0x1F501;
		unicode[3] = 0x1F501;
		unicode[4] = 0x1F53D;
		unicode[5] = 0x1F4F2;
		unicode[6] = 0x1F4A1;
		unicode[7] = 0x1F51C;
		unicode[8] = 0x1F501;
		unicode[9] = 0x2B06;
		unicode[10] = 0x2B07;
		unicode[11] = 0x1F50B;
		unicode[12] = 0x270B;
		unicode[14] = 0x1F51C;
		unicode[15] = 0x1F51C;
		unicode[16] = 0x1F6B6;
		unicode[17] = 0x1F6B6;
		unicode[18] = 0x1F50C;
		unicode[19] = 0x1F60A;
		unicode[20] = 0x1F483;
		unicode[21] = 0x1F468;



		// get a reference to the sensormanager
		SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		if (sm != null) {
			List<Sensor> listSensor = sm.getSensorList(Sensor.TYPE_ALL);
			List<String> listSensorType = new ArrayList<String>();

			for (int j = 0; j < listSensor.size(); j++)
			{
				listSensorType.add(getEmojiByUnicode(unicode[j]) + " " + listSensor.get(j).getName() + " Power-" + listSensor.get(j).getPower()
						+ " Delay-" + listSensor.get(j).getMinDelay() +  " Type =" + listSensor.get(j).getType());
			}

		    setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listSensorType));
			getListView().setTextFilterEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public String getEmojiByUnicode(int unicode){
		return new String(Character.toChars(unicode));
	}

}
