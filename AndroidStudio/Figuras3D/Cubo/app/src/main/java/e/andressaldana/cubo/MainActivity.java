package e.andressaldana.cubo;

import android.os.Bundle;
import android.app.Activity;
public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Canva l = new Canva(this);
        setContentView(l);
    }
}