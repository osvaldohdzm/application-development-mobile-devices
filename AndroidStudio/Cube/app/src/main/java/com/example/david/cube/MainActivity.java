package com.example.david.cube;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import static android.util.Half.EPSILON;

public class MainActivity extends AppCompatActivity {
    Perspectiva l;

    private float[] gyroQuaternion = new float[4];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        l = new Perspectiva(this);
        setContentView(l);
    }

    public class Perspectiva extends View implements SensorEventListener {
        int centerX, centerY, maxX, maxY, minMaxXY;
        Dimension dim;
        Obj obj = new Obj();
        Paint p = new Paint();
        Sensor mAccelerometer;
        SensorManager sensorManager;
        int x = 180, y = 180;

        public Perspectiva(Context c) {
            super(c);
            sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            mAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            sensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
            p.setAntiAlias(true);
        }

        private void CalculateRotationVectorFromGyro(SensorEvent event, float [] gyro) {

            // This timestep's delta rotation to be multiplied by the current rotation
            // after computing it from the gyro sample data.
            if (timestamp != 0) {
                final float dT = (event.timestamp - timestamp) * NS2S;
                // Axis of the rotation sample, not normalized yet.
                float axisX = gyro[0];
                float axisY = gyro[1];
                float axisZ = gyro[2];

                // Calculate the angular speed of the sample
                float omegaMagnitude = (float) Math.sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);

                // Normalize the rotation vector if it's big enough to get the axis
                // (that is, EPSILON should represent your maximum allowable margin of error)
                if (omegaMagnitude > EPSILON) {
                    axisX /= omegaMagnitude;
                    axisY /= omegaMagnitude;
                    axisZ /= omegaMagnitude;
                }

                // Integrate around this axis with the angular speed by the timestep
                // in order to get a delta rotation from this sample over the timestep
                // We will convert this axis-angle representation of the delta rotation
                // into a quaternion before passing it to GLM in native.
                float thetaOverTwo = omegaMagnitude * dT / 2.0f;
                float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
                float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
                gyroQuaternion[0] = cosThetaOverTwo;
                gyroQuaternion[1] = sinThetaOverTwo * axisX;
                gyroQuaternion[2] = sinThetaOverTwo * axisY;
                gyroQuaternion[3] = sinThetaOverTwo * axisZ;

            }

            // save timestamp for next call
            timestamp = event.timestamp;

        }

        protected void onDraw(Canvas c) {
            super.onDraw(c);
            p.setColor(Color.rgb(255, 255, 255)); // COLOR DE FONDO
            c.drawPaint(p);
            dim = new Dimension(getWidth() / 2, getHeight());
            maxX = dim.width - 1;
            maxY = dim.height - 1;
            minMaxXY = Math.min(maxX, maxY);
            centerX = maxX / 2;
            centerY = maxY / 2;
            obj.d = obj.rho * minMaxXY / obj.objSize;
            obj.eyeAndScreen();
            line(c, 0, 1);
            line(c, 1, 2);
            line(c, 2, 3);
            line(c, 3, 0); // aristas horizontales inferiores
            line(c, 4, 5);
            line(c, 5, 6);
            line(c, 6, 7);
            line(c, 7, 4); // aristas horizontales superiores
            line(c, 0, 4);
            line(c, 1, 5);
            line(c, 2, 6);
            line(c, 3, 7); // aristas verticales


            dim = new Dimension((int) (getWidth() * 1.5), getHeight());
            maxX = dim.width - 1;
            maxY = dim.height - 1;
            minMaxXY = Math.min(maxX, maxY);
            centerX = maxX / 2;
            centerY = maxY / 2;
            obj.d = obj.rho * minMaxXY / obj.objSize;
            obj.eyeAndScreen();
            line(c, 0, 1);
            line(c, 1, 2);
            line(c, 2, 3);
            line(c, 3, 0); // aristas horizontales inferiores
            line(c, 4, 5);
            line(c, 5, 6);
            line(c, 6, 7);
            line(c, 7, 4); // aristas horizontales superiores
            line(c, 0, 4);
            line(c, 1, 5);
            line(c, 2, 6);
            line(c, 3, 7); // aristas verticales
        }

        int iX(float x) {
            return (int) Math.floor(centerX + x);
        }

        int iY(float y) {
            return (int) Math.floor(centerY - y);
        }

        void line(Canvas ca, int i, int j) {
            p.setColor(Color.rgb(0, 0, 0));
            Point2D po = obj.vScr[i], q = obj.vScr[j];
            ca.drawLine(iX(po.x), iY(po.y), iX(q.x), iY(q.y), p);
        }

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            float[] rotationMatrix = new float[16];
            SensorManager.getRotationMatrixFromVector(rotationMatrix,
                    sensorEvent.values);
            // Remap coordinate system
            float[] remappedRotationMatrix = new float[16];
            SensorManager.remapCoordinateSystem(rotationMatrix,
                    SensorManager.AXIS_X,
                    SensorManager.AXIS_Z,
                    remappedRotationMatrix);
            // Convert to orientations
            float[] orientations = new float[3];
            SensorManager.getOrientation(remappedRotationMatrix, orientations);
            for (int i = 0; i < 3; i++) {
                orientations[i] = (float) (Math.toDegrees(orientations[i]));
            }
            obj.theta = (float) (getWidth() / 2) / (x + orientations[0]);
            obj.phi = (float) getHeight() / (y + orientations[1]);
            obj.rho = (obj.phi / obj.theta) * getHeight();
            centerX = (int) (x + orientations[0]);
            centerY = (int) (y + orientations[1]);

            invalidate();
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }
}
