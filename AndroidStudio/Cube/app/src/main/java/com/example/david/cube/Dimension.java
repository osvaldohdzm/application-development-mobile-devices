package com.example.david.cube;

class Dimension {
    int width;
    int height;
    public Dimension(int w, int h) {
        width = w;
        height = h;
    }
}