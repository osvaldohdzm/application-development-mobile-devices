    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba;

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

/**
 * @author JanetNaibi
 */
public class Midlet extends MIDlet {
    Form        f;     
    Display     d;     
    StringItem  si; 
    
    public Midlet(){         
        f = new Form("Prueba");         
        d = Display.getDisplay(this);         
        si= new StringItem("", "Bienvenido a ESCOM");         
        f.append(si);     
    }

    public void startApp() {
         d.setCurrent(f);
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    }
}
