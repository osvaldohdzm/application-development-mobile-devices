package lab.numeros;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

public class FuncionesNumeros extends MIDlet implements CommandListener {

	private Form form;
    private Display display;
    private StringItem item;
    private int numeroPrueba;
    private String cadenaPrueba;
    private String opciones[];
    private List menu;
	
	public FuncionesNumeros() {
		
        cadenaPrueba="reconocer";
        this.numeroPrueba=7;
        this.form=new Form("Numero maravilloso");
        this.display= Display.getDisplay(this);
	}

	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		

	}

	protected void pauseApp() {
		throw new UnsupportedOperationException("Not supported yet."); 

	}

	protected void startApp() throws MIDletStateChangeException {
		
		
		  item= new StringItem("","Primera practica: ");
	        opciones=new String[4];
	        opciones[0]="Numero maravilloso";
	        opciones[1]="Fibonacci";
	        opciones[2]="Cadena palindromo";
	        opciones[3]="Numero primo";
	        
	        menu = new List("Opciones",List.IMPLICIT,opciones,null);
	        display.setCurrent(menu);
	        
	        menu.setCommandListener(this);

	}

	  
    public void commandAction(Command c, Displayable d) {
        if(d == menu){
            switch(menu.getSelectedIndex()){
                case 0:
                    ventanaOpcionAction("Numero maravilloso");
                    break;
                case 1:
                    ventanaOpcionAction("Fibonacci");
                    break;
                case 2:
                    ventanaOpcionAction("Cadena palindromo");
                    break;
                case 3:
                    ventanaOpcionAction("Numero Primo");
                    break;
            }
        }
        
     
    }
    
    
    private boolean isNumeroMaravilloso(int numero,Form form){
        String estadosNumericos="";
        do{
            if(numero%2==0){
                numero=numero/2;
                estadosNumericos+=""+numero+"\n";
            }else{
                numero=(numero*3)+1;
                estadosNumericos+=""+numero+"\n";
            }
            System.out.println(numero);
        }while(numero!=1);
       
        item= new StringItem("","\nEstados: \n"+estadosNumericos);
        form.append(item);
        
        return true;
    }
    
    private boolean isFibonnaci(int numero,Form form){
        int suma;
        int j = 1,l = 0;
        String estadosNumericos="";
        
        for(;;){	
            suma=l+j;
            if(j==numero){
                item= new StringItem("","\nEstados: \n"+estadosNumericos);
                form.append(item);
                return true;
            }  
            else
                if(j>numero){
                    item= new StringItem("","\nEstados: \n"+estadosNumericos);
                    form.append(item);
                    return false;
                }   
            estadosNumericos+=""+j+"\n";
            l=j;
            j=suma;
	}
    }
    
    private boolean isPrimo(int numero){
	int contadorDeDivision=0;
	
	if(numero==1 || numero==0)
            return false;
	else		
            for(int i=numero;i>=1;i--){
                if(numero%i==0)
                    contadorDeDivision++;
            }		
	return (contadorDeDivision>2)?false:true;
    }
    
    private boolean isVerificacionPalindromo(String texto)
    {
        boolean palindromoCorrecto = false;
            
        for (int i = 0; i < ((texto.length())/2); i++) {
            if(texto.charAt(i)==texto.charAt(texto.length()-i-1)) 
                palindromoCorrecto=true;
            else{
                    palindromoCorrecto=false;
                    break;
                }
        }
        return palindromoCorrecto;
    }
     
    private void ventanaOpcionAction(String opcion){
        this.form=new Form(opcion);
        this.item=new StringItem("","Opcion: "+opcion);
        form.append(item);
        
        if(opcion.equals("Numero maravilloso")){
            if(this.isNumeroMaravilloso(numeroPrueba, form))
                item=new StringItem("","\nEl numero: "+numeroPrueba+" es maravilloso");
            else
                item=new StringItem("","\nEl numero: "+numeroPrueba+" No es maravilloso");
            form.append(item);
        }else if(opcion.equals("Fibonacci")){
            if(this.isFibonnaci(numeroPrueba, form))
                item=new StringItem("","\nEl numero: "+numeroPrueba+" es un fibonacci");
            else
                item=new StringItem("","\nEl numero: "+numeroPrueba+" No pertenece al fibonacci");
            form.append(item);
        }else if(opcion.equals("Numero Primo")){
            if(this.isPrimo(numeroPrueba))
                item=new StringItem("","\nEl numero: "+numeroPrueba+" es primo");
            else
                item=new StringItem("","\nEl numero: "+numeroPrueba+" No es primo");
            form.append(item);
        }else{
            if(this.isVerificacionPalindromo(cadenaPrueba))
                item=new StringItem("","\nLa palabra: '"+cadenaPrueba+"' es palindroma");
            else
                item=new StringItem("","\nLa palabra: '"+cadenaPrueba+"' No es palindroma");
            form.append(item);
        }
        this.display.setCurrent(form);
    }

}
