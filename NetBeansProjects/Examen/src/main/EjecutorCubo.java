package main;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.*;
import modelo.Paralelepipedo;


public class EjecutorCubo extends MIDlet implements CommandListener {

    private Display display;
    private List menu;
    private String[] titulos;
    private StringItem  itemString;
    private String contactosAlmacenados;
    private Paralelepipedo figura1;
    
    
    private static int volumen;

   
    public void startApp() {
        titulos=new String[2];
        display = Display.getDisplay(this);
        titulos[0]="Datos";
        titulos[1]="Gráfico";
        
        menu = new List("Opciones",List.IMPLICIT,titulos,null);
        menu.setCommandListener(this);
        display.setCurrent(menu);
        
        
    }
    
    public void pauseApp() {
        
    }    
    
    public void destroyApp(boolean unconditional) {   
    
    }

    public void commandAction(Command c, Displayable d) {
        
        
        
        if(d==menu){
           switch(menu.getSelectedIndex()){
                              
               case 0:
                   this.display.setCurrent(new FormIngresoDatos("Calculo del volumen",this));   
                   volumen = FormIngresoDatos.getResultadoVolumen();
                   break;
               case 1:
                   this.display.setCurrent(new FormMostrarCubo(this, getFigura1(), volumen).getDibujarParalapipedo() );
                   break;
           }
       }
    }
    
    public void setMenuPrincipal(Form formActual){
        this.display.setCurrent(menu);
        formActual=null;
    }

    public Paralelepipedo getFigura1() {
        return figura1;
    }

    public void setFigura1(Paralelepipedo figura1) {
        this.figura1 = figura1;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }
    
}
