/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.Graphics;
import main.EjecutorCubo;
import modelo.Obj;
import modelo.Paralelepipedo;
import modelo.Point2D;
/**
 *
 * @author luisGerardo
 */
public class FormMostrarCubo{
    
    private EjecutorCubo ejecutor;
    private Paralelepipedo paralelo;
    public int volumen;
    
    public FormMostrarCubo(EjecutorCubo ejecutor,Paralelepipedo paralelo, int v) {
        this.ejecutor=ejecutor;
        this.paralelo=paralelo;
        this.volumen = v;
    }
    
    public Canvas getDibujarParalapipedo(){
        Canvas canvas = new Canvas(){           
            private Obj obj;
            private int centerX=0, centerY=0, maxX=0, maxY=0, minMaxXY=0;
            
            protected void paint(Graphics g) {
                obj=new Obj();
                
                g.setColor(0xE7F739);
                g.fillRect(0, 0, getWidth(), getHeight());
                maxX = getWidth() - 1;
                maxY = getHeight() - 1;
                minMaxXY = Math.min(maxX, maxY);
                centerX = maxX / 2;
                centerY = maxY / 2;
                obj.d = obj.rho * minMaxXY / obj.objSize;
                obj.eyeAndScreen();
                
                line(g, 0, 1);
                line(g, 1, 2);
                line(g, 2, 3);
                line(g, 3, 0); // aristas horizontales inferiores
                line(g, 4, 5);
                line(g, 5, 6);
                line(g, 6, 7);
                line(g, 7, 4); // aristas horizontales superiores
                line(g, 0, 4);
                line(g, 1, 5);
                line(g, 2, 6);
                line(g, 3, 7); // aristas verticales
                
                // Ejes
                /*
                int PAD = 20;
                int w = getWidth();
                int h = getHeight();
                g.drawLine(PAD, PAD, PAD, h-PAD);
                g.drawLine(PAD, h-PAD, w-PAD, h-PAD);
                 */                

                g.setFont(Font.getFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL));
                g.drawString("Volumen : " + volumen, 40, 10, Graphics.HCENTER|Graphics.BASELINE);  
                
            }
            
            public void line(Graphics g, int i, int j) {
                Point2D p = obj.vScr[i],q = obj.vScr[j];
                g.setColor(0, 0, 0);
                g.drawLine(centerX + (int) p.x, centerY - (int) p.y, centerX + (int) q.x, centerY - (int) q.y);
            }  
        };
        return canvas;
    }
}

   
