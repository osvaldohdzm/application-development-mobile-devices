/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import modelo.Paralelepipedo;

/**
 *
 * @author luisGerardo
 */
public class FormIngresoDatos extends Form implements CommandListener {

    private EjecutorCubo principal;
    private TextField vector1[];
    private TextField vector2[];
    private TextField vector3[];
    private StringItem itemText;
    private Command comandoGuardar, comandoSalir;
    private Alert alert;
    
    public int vectorInt1[] = new int[3];
    public int vectorInt2[] = new int[3];
    public int vectorInt3[] = new int[3];
    
    static int resultadoVolumen;

    public static int getResultadoVolumen() {
        return resultadoVolumen;
    }

    public void setResultadoVolumen(int resultadoVolumen) {
        this.resultadoVolumen = resultadoVolumen;
    }

    public FormIngresoDatos(String title, EjecutorCubo principal) {
        super(title);
        this.principal = principal;
        vector1 = new TextField[3];
        vector2 = new TextField[3];
        vector3 = new TextField[3];
        String index = "X";

        for (int i = 0; i < vector1.length; i++) {

            if (i == 0) {
                index = "X";
            } else if (i == 1) {
                index = "Y";
            } else if (i == 2) {
                index = "Z";
            }

            itemText = new StringItem("", "Vector " + index + ":  ");
            this.append(itemText);
            for (int j = 0; j < vector1.length; j++) {
                switch (i) {
                    case 0:
                        vector1[j] = new TextField(index + i + ": ", "", 10, TextField.ANY);
                        this.append(vector1[j]);
                        break;
                    case 1:
                        vector2[j] = new TextField(index + i + ": ", "", 10, TextField.ANY);
                        this.append(vector2[j]);
                        break;
                    case 2:
                        vector3[j] = new TextField(index + i + ": ", "", 10, TextField.ANY);
                        this.append(vector3[j]);
                        break;
                    default:
                        System.out.println("Array desbordado");
                }
            }
        }
        
        this.comandoGuardar = new Command("Guardar", Command.EXIT, 1);
        this.comandoSalir = new Command("Salir", Command.EXIT, 1);

        this.addCommand(comandoSalir);
        this.addCommand(comandoGuardar);
        this.setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == this.comandoSalir) {
            this.principal.setMenuPrincipal(this);
        }

        if (c == this.comandoGuardar) {
            

            //CARGANDO DATOS A LA NUEVA FIGURA;
            for (int i = 0; i < vector1.length; i++) {
                for (int j = 0; j < vector1.length; j++) {
                    switch (i) {
                        case 0:
                            vectorInt1[j] = Integer.parseInt(vector1[j].getString());
                            break;
                        case 1:
                            vectorInt2[j] = Integer.parseInt(vector2[j].getString());
                            break;
                        case 2:
                            vectorInt3[j] = Integer.parseInt(vector3[j].getString());
                            break;
                        default:
                            System.out.println("Desbordamineto del array");
                    }
                }
            }

            Paralelepipedo figura1 = new Paralelepipedo(vectorInt1, vectorInt2, vectorInt3);
            resultadoVolumen = figura1.getVolumenParalepipedo();

            if (resultadoVolumen != -1) {
                alert = new Alert("Nota", "Volumen: " + resultadoVolumen, null, AlertType.CONFIRMATION);
                alert.setTimeout(5000);
                principal.getDisplay().setCurrent(this.alert, this);
                principal.setFigura1(figura1);
            } else {
                alert = new Alert("Nota", "El volumen no puede ser calculado", null, AlertType.CONFIRMATION);
                alert.setTimeout(5000);
                principal.getDisplay().setCurrent(this.alert, this);
            }
        }
    }

}
