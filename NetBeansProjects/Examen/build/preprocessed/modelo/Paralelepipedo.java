/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

public class Paralelepipedo {
    private int vector1[];
    private int vector2[];
    private int vector3[];
    
    public Paralelepipedo(int[] vector1, int[] vector2, int[] vector3) {
        this.vector1 = vector1;
        this.vector2 = vector2;
        this.vector3 = vector3;
    }

    public Paralelepipedo() {
        this(new int[3],new int[3],new int[3]);
    }

    public int[] getVector1() {
        return vector1;
    }

    public void setVector1(int[] vector1) {
        this.vector1 = vector1;
    }

    public int[] getVector2() {
        return vector2;
    }

    public void setVector2(int[] vector2) {
        this.vector2 = vector2;
    }

    public int[] getVector3() {
        return vector3;
    }

    public void setVector3(int[] vector3) {
        this.vector3 = vector3;
    }
    
    public int getVolumenParalepipedo(){
        int volumenTotal=0;
        
        if(this.getVector3()!=null && this.getVector2()!=null && this.getVector1()!=null){      
            for(int i=0;i<vector1.length;i++){
                switch(i){
                    case 0:
                        volumenTotal+=this.vector1[i]*((this.vector2[1]*this.vector3[2])-(this.vector2[2]*this.vector3[1]));
                        break;
                    case 1:
                        volumenTotal+=-(this.vector1[i]*((this.vector2[0]*this.vector3[2])-(this.vector2[2]*this.vector3[0])));
                        break;
                    case 2:
                        volumenTotal+=this.vector1[i]*((this.vector2[0]*this.vector3[1])-(this.vector2[1]*this.vector3[0]));
                        break;   
                }      
            }
            return volumenTotal;
            
        }
        System.out.println("NO SE PUEDE CALCULAR EL VOLUMEN DEL PARALELEPIPEDO");
        return -1;
    }
}
    

