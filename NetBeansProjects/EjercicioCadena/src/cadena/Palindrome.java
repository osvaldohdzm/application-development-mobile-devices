/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadena;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;

/**
 * @author JanetNaibi
 */
public class Palindrome extends MIDlet implements CommandListener, ItemCommandListener {
    
    private static final Command CMD_EXIT = new Command("Exit", Command.EXIT, 1);
    private Display display;
    private Form mainForm;

    protected void startApp() {
        display = Display.getDisplay(this);

        mainForm = new Form("Ejercicio 4");

        String salida;
        String palabra = "anitalavalatina"; //notracesenesecarton - anitalavalatina
        String palabra1 = "";


        // Palindromo *****************************
        int i; 
        for (i = palabra.length() - 1 ; i >= 0; i--) { 
           palabra1 = palabra1 + palabra.charAt(i); 
        } 
        if (palabra.equals(palabra1)) { 
            salida = "\nLa cadena es un palindrome";  
        }
        else{ salida = "\nLa cadena no es un palindrome";  
        }
        StringItem item = new StringItem("Cadena:", palabra + " " + salida);
        mainForm.append(item); 

            mainForm.addCommand(CMD_EXIT);
            mainForm.setCommandListener(this);
            display.setCurrent(mainForm);
       
    }

    public void commandAction(Command c, Item item) {

    }

    public void commandAction(Command c, Displayable d) {
        destroyApp(false);
        notifyDestroyed();
    }

    protected void destroyApp(boolean unconditional) {
    }

    protected void pauseApp() {
    }
}
