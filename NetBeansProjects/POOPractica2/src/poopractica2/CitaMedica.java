package poopractica2;

/**
 *
 * @author Osvaldo Hernández Morales
 */
public class CitaMedica {
private String nombrePaciente;
private int Edad;
private String domicilio;
private char genero;
private long telefonoFijo;
private long numSeguroSocial;
private String tipoSangre;
private String alergias;
private String horaCita;
private int consultorio;
private String doctor;
private String padecimiento;    
  

    public CitaMedica(String nombrePaciente, long numSeguroSocial) {
        this.nombrePaciente = nombrePaciente;
        this.numSeguroSocial = numSeguroSocial;
        this.domicilio = "No registrado";
        this.tipoSangre = "No registrado";
        this.alergias = "No registrado";
        this.horaCita = "No registrado";
        this.doctor = "No registrado";
        this.padecimiento = "No registrado";
    }

    public CitaMedica(String nombrePaciente, int Edad, long numSeguroSocial) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.numSeguroSocial = numSeguroSocial;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long numSeguroSocial) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.numSeguroSocial = numSeguroSocial;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, long numSeguroSocial) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.numSeguroSocial = numSeguroSocial;
    }


    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
    }

    
    
    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre, String alergias) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
        this.alergias = alergias;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre, String alergias, String horaCita) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
        this.alergias = alergias;
        this.horaCita = horaCita;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre, String alergias, String horaCita, int consultorio) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
        this.alergias = alergias;
        this.horaCita = horaCita;
        this.consultorio = consultorio;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre, String alergias, String horaCita, int consultorio, String doctor) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
        this.alergias = alergias;
        this.horaCita = horaCita;
        this.consultorio = consultorio;
        this.doctor = doctor;
    }

    public CitaMedica(String nombrePaciente, int Edad, String domicilio, char genero, long telefonoFijo, long numSeguroSocial, String tipoSangre, String alergias, String horaCita, int consultorio, String doctor, String padecimiento) {
        this.nombrePaciente = nombrePaciente;
        this.Edad = Edad;
        this.domicilio = domicilio;
        this.genero = genero;
        this.telefonoFijo = telefonoFijo;
        this.numSeguroSocial = numSeguroSocial;
        this.tipoSangre = tipoSangre;
        this.alergias = alergias;
        this.horaCita = horaCita;
        this.consultorio = consultorio;
        this.doctor = doctor;
        this.padecimiento = padecimiento;
    }

    
    
    /**
     * @return the nombrePaciente
     */
    public String getNombrePaciente() {
        return nombrePaciente;
    }

    /**
     * @param nombrePaciente the nombrePaciente to set
     */
    public void setNombrePaciente(String nombrePaciente) {
        this.nombrePaciente = nombrePaciente;
    }

    /**
     * @return the Edad
     */
    public int getEdad() {
        return Edad;
    }

    /**
     * @param Edad the Edad to set
     */
    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    /**
     * @return the domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @return the genero
     */
    public char getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(char genero) {
        this.genero = genero;
    }

    /**
     * @return the telefonoFijo
     */
    public long getTelefonoFijo() {
        return telefonoFijo;
    }

    /**
     * @param telefonoFijo the telefonoFijo to set
     */
    public void setTelefonoFijo(long telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    /**
     * @return the numSeguroSocial
     */
    public long getNumSeguroSocial() {
        return numSeguroSocial;
    }

    /**
     * @param numSeguroSocial the numSeguroSocial to set
     */
    public void setNumSeguroSocial(long numSeguroSocial) {
        this.numSeguroSocial = numSeguroSocial;
    }

    /**
     * @return the tipoSangre
     */
    public String getTipoSangre() {
        return tipoSangre;
    }

    /**
     * @param tipoSangre the tipoSangre to set
     */
    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    /**
     * @return the alergias
     */
    public String getAlergias() {
        return alergias;
    }

    /**
     * @param alergias the alergias to set
     */
    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    /**
     * @return the horaCita
     */
    public String getHoraCita() {
        return horaCita;
    }

    /**
     * @param horaCita the horaCita to set
     */
    public void setHoraCita(String horaCita) {
        this.horaCita = horaCita;
    }

    /**
     * @return the consultorio
     */
    public int getConsultorio() {
        return consultorio;
    }

    /**
     * @param consultorio the consultorio to set
     */
    public void setConsultorio(int consultorio) {
        this.consultorio = consultorio;
    }

    /**
     * @return the doctor
     */
    public String getDoctor() {
        return doctor;
    }

    /**
     * @param doctor the doctor to set
     */
    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    /**
     * @return the padecimiento
     */
    public String getPadecimiento() {
        return padecimiento;
    }

    /**
     * @param padecimiento the padecimiento to set
     */
    public void setPadecimiento(String padecimiento) {
        this.padecimiento = padecimiento;
    }
}
