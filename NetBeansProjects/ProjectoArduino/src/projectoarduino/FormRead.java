/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectoarduino;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author luisGerardo
 */
public class FormRead extends Form implements CommandListener {

    private Main principal;
    private Display mDisplay;
    private Command exitCmd = new Command("Salir", Command.EXIT, 0);
    private Command backCmd = new Command("Regresar", Command.BACK, 0);

    StreamConnection conn = null;
    InputStream is = null;
    OutputStream os = null;
    String message = null;

    byte buffer[] = new byte[24];

    public FormRead(String title, Main principal) {
        super(title);
        this.principal = principal;

        try {
            conn = (StreamConnection) Connector.open("btspp://98D332118624:1;authenticate=false;encrypt=false;master=false", Connector.READ_WRITE);

            // First message
            os = conn.openOutputStream();
            os.write(0);

            /*
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
            }
             */
            is = conn.openInputStream();

            is.read(buffer);
            message = new String(buffer);

            this.append(new StringItem(null, "\n" + message));

        } catch (IOException io) {
            this.append(new StringItem(null, "\n" + "Error"));
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (IOException ignored) {
                }
            }

            if (os != null) {
                try {
                    os.close();
                } catch (IOException ignored) {
                }
            }

            if (is != null) {
                try {
                    is.close();
                } catch (IOException ignored) {
                }
            }
        }

        this.addCommand(backCmd);
        this.addCommand(exitCmd);
        this.setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == this.backCmd) {
            this.principal.setMenuPrincipal(this);
        } else if (c == this.exitCmd) {
            this.principal.destroyApp(true);
            this.principal.notifyDestroyed();
        }

    }

}
