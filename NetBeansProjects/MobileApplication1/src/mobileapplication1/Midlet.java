/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mobileapplication1;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.*;

/**
 * @author osvaldohm
 */
public class Midlet extends MIDlet {
    
    
    Command c;
    Display d;
    Form f;
    
    public Midlet(){
        d=Display.getDisplay(this);
        c=new Command("Salir",Command.EXIT,2);
        f=new Form("Hola mundo");
        f.addCommand(c);
    }

    public void startApp()throws MIDletStateChangeException {
        d.setCurrent(f);
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    }    

}
