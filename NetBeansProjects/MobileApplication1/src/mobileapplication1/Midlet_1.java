package mobileapplication1;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.MIDlet;

public class Midlet_1 extends MIDlet implements CommandListener, ItemCommandListener {

    private static final Command CMD_EXIT = new Command("Exit", Command.EXIT, 1);
    private Display display;
    private Form mainForm;

    protected void startApp() {
        display = Display.getDisplay(this);

        mainForm = new Form("Ejercicio 1");

        int I;
        int numero = 5;
        String salida;
        int contador = 0;
        String palabra = "anitalavalatia";
        String palabra1 = "";

        // Numero primo *****************************
        for (I = 1; I <= numero; I++) {
            if ((numero % I) == 0) {
                contador++;
            }
        }
        if (contador <= 2) {
            salida = "Es primo";
        } else {
            salida = "No es primo";
        }
        StringItem item
                = new StringItem("Número primo:", numero + " " + salida);
        mainForm.append(item);

        // Numero fibonacci *****************************
        int f1, f2, i, resultado=0;
        f1 = 0;
        f2 = 1;
        for (;resultado < numero;) {
            resultado = f1 + f2;
            f1 = f2;
            f2 = resultado;           
        }   
         if (numero == resultado) {
                salida = "si es número fibonacci";
            } else {
                salida = "no es número fibonacci";
            }
         item
                = new StringItem("Número fibonacci:", numero + " " + salida);
        mainForm.append(item);   
        
        
         // Palindromo *****************************
        for (i = palabra.length() - 1 ; i >= 0; i--) { 
           palabra1 = palabra1 + palabra.charAt(i); 
        } 
        if (palabra.equals(palabra1)) { 
            salida = "es palindromo";  
        }
        else{ salida = "no es palindromo";  
        }
         item
                = new StringItem("Palíndromo:", palabra + " " + salida);
        mainForm.append(item); 

        // Maravilloso *****************************
         for (; numero == 1;) {
            if ((numero % 2) == 0) {
                numero = numero / 2;
            }
        }
        if (contador <= 2) {
            salida = "Es primo";
        } else {
            salida = "No es primo";
        }
        
         item
                = new StringItem("Número maravilloso:", palabra + " " + salida);
        mainForm.append(item); 

        
            mainForm.addCommand(CMD_EXIT);
            mainForm.setCommandListener(this);
            display.setCurrent(mainForm);
        
            

    }

    public void commandAction(Command c, Item item) {

    }

    public void commandAction(Command c, Displayable d) {
        destroyApp(false);
        notifyDestroyed();
    }

    protected void destroyApp(boolean unconditional) {
    }

    protected void pauseApp() {
    }
}
