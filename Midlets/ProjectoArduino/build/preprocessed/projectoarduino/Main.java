package projectoarduino;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.midlet.*;

public class Main extends MIDlet implements CommandListener {

    public static Display display;
    private List menu;
    private String[] titulos;
    private Command exitCmd = new Command("Salir", Command.EXIT, 0);

    public void startApp() {
        display = Display.getDisplay(this);
        titulos = new String[1];
        titulos[0] = "Leer sensor";

        menu = new List("Opciones", List.IMPLICIT, titulos, null);
        menu.addCommand(exitCmd);
        menu.setCommandListener(this);
        display.setCurrent(menu);

    }

    public void pauseApp() {

    }

    public void destroyApp(boolean unconditional) {

    }

    public void commandAction(Command c, Displayable d) {

        if (c == exitCmd) {
            destroyApp(true);
            notifyDestroyed();
        } else {

            switch (menu.getSelectedIndex()) {
                case 0:
                    this.display.setCurrent(new FormRead("Conección con Arduino", this));
                    break;
            }
        }

    }

    public void setMenuPrincipal(Form formActual) {
        this.display.setCurrent(menu);
        formActual = null;
    }

    public Display getDisplay() {
        return display;
    }

    public void setDisplay(Display display) {
        this.display = display;
    }

}
