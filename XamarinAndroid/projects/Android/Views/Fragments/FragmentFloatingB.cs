﻿using Android.Content;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class FragmentFloatingB : Android.Support.V4.App.Fragment
    {
        private TextView bienvenida;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment4, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            bienvenida = view.FindViewById<TextView>(Resource.Id.textBienvenida);
            FloatingActionButton fabicon = view.FindViewById<FloatingActionButton>(Resource.Id.fab);

            fabicon.Click += (sender, e) => {
                bienvenida.Text = "Programacion movil";
                Toast.MakeText(view.Context, "Boton en pantalla", ToastLength.Short).Show();
                fabicon.SetX(50);
                fabicon.SetY(300);
                fabicon.RefreshDrawableState();
            };


            return view;
        }

    }
}