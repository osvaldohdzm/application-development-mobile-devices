﻿using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class FragmentIntents : Android.Support.V4.App.Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment2, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);


            return view;
        }

    }
}