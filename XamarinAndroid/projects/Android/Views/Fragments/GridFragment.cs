﻿
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace AsuraEducationPlatform.Droid.Views
{
    public class GridFragment : Android.Support.V4.App.Fragment
    {
        
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static GridFragment NewInstance()
        {
            var frag2 = new GridFragment { Arguments = new Bundle() };
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.grid_layout, container, false);

            

            return view;
        }
    }
}