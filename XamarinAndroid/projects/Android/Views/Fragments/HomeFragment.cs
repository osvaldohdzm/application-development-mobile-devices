﻿using Android;
using Android.OS;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
 
 
using System;
 
using System.Collections.Generic;

namespace AsuraEducationPlatform.Droid.Views
{
    public class HomeFragment : Android.Support.V4.App.Fragment
    {
       

        public static HomeFragment NewInstance()
        {
            var frag1 = new HomeFragment
            {
                Arguments = new Bundle()
            };
            return frag1;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.home_fragment, container, false);

            TextView text1 = view.FindViewById<TextView>(Resource.Id.textView1);
            text1.Text = "Hola mundo";

            return view;
        }           

    }
}