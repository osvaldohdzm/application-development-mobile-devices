﻿using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class FragmentNumeros : Android.Support.V4.App.Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment2, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            Button btnFibonacci = view.FindViewById<Button>(Resource.Id.btnFibo);
            Button btnMaravilloso = view.FindViewById<Button>(Resource.Id.btnmaravilloso);
            Button btnpalindrome = view.FindViewById<Button>(Resource.Id.btnPalindromo);
            Button btnPrimo = view.FindViewById<Button>(Resource.Id.btnprimo);
            TextView dato = view.FindViewById<TextView>(Resource.Id.txtDato);
            TextView resultado = view.FindViewById<TextView>(Resource.Id.txtResultado);
                    
            btnFibonacci.Click += delegate
            {
                resultado.Text= OperationsNumbers(1, dato.Text, view.Context);
            };
            btnMaravilloso.Click += delegate
            {
                resultado.Text = OperationsNumbers(4, dato.Text, view.Context);
            };
            btnpalindrome.Click += delegate
            {
                resultado.Text = OperationsNumbers(3, dato.Text, view.Context);
            };
            btnPrimo.Click += delegate
            {
                resultado.Text = OperationsNumbers(2, dato.Text, view.Context);
            };


            return view;
        }

        private string OperationsNumbers(int operation, string dato, Context context)
        {
            int number = 0;
                      

            if (operation == 1)
            {
                number = ToNumberValidation(dato,context);

                if (IsFibonacci(number))
                {
                    return "El numero esta en la serie fibonacci";
                }
                else
                {
                    return "El numero no esta en la serie fibonacci";
                }
            }

            if (operation == 2)
            {
                number = ToNumberValidation(dato, context);
                if (IsPrimo(number))
                {
                    return "El numero es primo";
                }
                else
                {
                    return "El numero no es primo";
                }
            }

            if (operation == 3)
            {
                if (IsPalindrome(dato))
                {
                    return "La frase es palíndromo";
                }
                else
                {
                    return "La frase no es palíndromo";
                }
            }

            if (operation == 4)
            {
                number = ToNumberValidation(dato, context);
                return WunderNumberList(number);
            }

            return "Sin resultado";
        }

        private int ToNumberValidation(string dato, Context context) {
            int number = 0;

            try
            {
                number = System.Convert.ToInt32(dato);
            }
            catch (FormatException)
            {
                Toast.MakeText(context, "Entrada invalida", ToastLength.Long).Show();
            }
            catch (OverflowException)
            {
                Toast.MakeText(context, "Desborde", ToastLength.Long).Show();
            }

            return number;
        }

        private string WunderNumberList(int number)
        {
            string numberStates = "\nEstados: ";
            do
            {
                if (number % 2 == 0)
                {
                    number = number / 2;
                    numberStates += "" + number + "\n";
                }
                else
                {
                    number = (number * 3) + 1;
                    numberStates += "" + number + "\n";
                }
            } while (number != 1);

            numberStates = "\n" + numberStates;
            return numberStates;
        }

        private bool IsFibonacci(int number) {
            int suma;
            int j = 1, l = 0;

            for (int i = 0; ; i++)
            {
                suma = l + j;
                if (j == number)
                    return true;
                else
                if (j > number)
                    return false;
                l = j;
                j = suma;
            }
        }

        private bool IsPrimo(int number) {
            int divisionCounter = 0;

            if (number == 1 || number == 0)
                return false;
            else
                for (int i = number; i >= 1; i--)
                {
                    if (number % i == 0)
                        divisionCounter++;
                }
            return (divisionCounter > 2) ? false : true;
        }

        private bool IsPalindrome(string phrase) {
            bool palindrome = false;

            for (int i = 0; i < ((phrase.Length) / 2); i++)
            {
                if (phrase[i] == phrase[phrase.Length - i - 1])
                    palindrome = true;
                else
                {
                    palindrome = false;
                    break;
                }
            }
            return palindrome;
        }

    }
}