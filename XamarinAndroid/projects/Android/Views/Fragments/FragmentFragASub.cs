﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class FragmentFragASub : Android.Support.V4.App.Fragment
    {
        private MediaPlayer mp;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.fragment7, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            Button btnSonido = view.FindViewById<Button>(Resource.Id.btnSonido);

            btnSonido.Click += (sender, e) => {
                mp = MediaPlayer.Create(view.Context, Resource.Raw.sonido);
                mp.Start();
            };

            return view;
        }

    }
}