﻿using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AsuraEducationPlatform.Droid.Views;
using Java.Interop;
using System;
using System.Collections.Generic;

namespace AsuraEducationPlatform.Views.Fragments
{
    public class FragmentBotones : Android.Support.V4.App.Fragment
    {
        View view;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            view = inflater.Inflate(Resource.Layout.fragment3, container, false);

            Button jbn1 = view.FindViewById<Button>(Resource.Id.xbn1);
            Button jbn2 = view.FindViewById<Button>(Resource.Id.xbn2);
            Button jbn3 = view.FindViewById<Button>(Resource.Id.xbn3);
            Button jbn4 = view.FindViewById<Button>(Resource.Id.xbn4);
            Button jbn5 = view.FindViewById<Button>(Resource.Id.xbtnActivity);

            jbn1.Click += delegate (object sender, EventArgs e) {
                Toast.MakeText(Context, "Botón digitado: xbn1\nUtiliza: clase btn1Listener", ToastLength.Long).Show();
            };
            jbn2.Click += (sender, e) => {
                Toast.MakeText(Context, "Botón digitado: xbn2\nUtiliza: new OnClickListener{}", ToastLength.Long).Show();
            };
            jbn3.Click += Jbn3_Click;
            jbn4.Click += Jbn4_Click;
            jbn5.Click += Jbn5_Click;

            return view;
        }

        private void Jbn4_Click(object sender, EventArgs e)
        {
            Toast.MakeText(view.Context, "Botón digitado: xbn4 utiliza XML", ToastLength.Long).Show();
        }

        private void Jbn5_Click(object sender, EventArgs e)
        {
            StartActivity(new Intent(view.Context, typeof(ActivityImageButton)));
        }

        private void Jbn3_Click(object sender, EventArgs e)
        {
            Toast.MakeText(view.Context, "Botón digitado: xbn3\nUtiliza: implements OnClickListener.", ToastLength.Long).Show();
        }
     
    }
}