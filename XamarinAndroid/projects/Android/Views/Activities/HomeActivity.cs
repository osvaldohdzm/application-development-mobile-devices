﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using AsuraEducationPlatform.Views.Fragments;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme", LaunchMode = LaunchMode.SingleTop, HardwareAccelerated = true)]
    public class HomeActivity : AppCompatActivity
    {
        private Android.Support.V4.App.FragmentManager mFragmentManager;
        private DrawerLayout drawerLayout;
        private NavigationView navigationView;
        private IMenuItem previousItem;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            mFragmentManager = SupportFragmentManager;


            SetContentView(Resource.Layout.home);
            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                SetSupportActionBar(toolbar);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetHomeButtonEnabled(true);
            }

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            //Set hamburger items menu
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);

            //Setup navigation view
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            //Hrandle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                if (previousItem != null)
                {
                    previousItem.SetChecked(false);
                }

                navigationView.SetCheckedItem(e.MenuItem.ItemId);

                previousItem = e.MenuItem;

                if (e.MenuItem.ItemId == Resource.Id.nav_home_1)
                {
                    ListItemClicked(1);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_2)
                {
                    ListItemClicked(2);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_3)
                {
                    ListItemClicked(3);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_4)
                {
                    ListItemClicked(4);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_5)
                {
                    ListItemClicked(5);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_6)
                {
                    ListItemClicked(6);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_7)
                {
                    ListItemClicked(7);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_8)
                {
                    ListItemClicked(8);
                }
                else if (e.MenuItem.ItemId == Resource.Id.nav_home_9)
                {
                    ListItemClicked(9);
                }


                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            if (savedInstanceState == null)
            {
                navigationView.SetCheckedItem(Resource.Id.nav_home_1);
                ListItemClicked(1);
            }
        }

        private int oldPosition = -1;
        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
            {
                return;
            }

            oldPosition = position;

            Android.Support.V4.App.Fragment fragment = null;
            switch (position)
            {
                case 1:
                    fragment = HomeFragment.NewInstance();
                    break;
                case 2:
                    fragment = new FragmentNumeros { Arguments = new Bundle() }; ;
                    break;
                case 3:
                    fragment = new FragmentPlantillas { Arguments = new Bundle() }; ;
                    break;
                case 4:
                    fragment = new FragmentBotones { Arguments = new Bundle() }; ;
                    break;
                case 5:
                    fragment = new FragmentFloatingB { Arguments = new Bundle() }; ;
                    break;
                case 6:
                    fragment = new FragmentSonidos { Arguments = new Bundle() }; ;
                    break;
                case 7:
                    fragment = new FragmentIntents { Arguments = new Bundle() }; ;
                    break;
                case 8:
                    fragment = new FragmentFragA { Arguments = new Bundle() }; ;
                    break;
                case 9:
                    fragment = new FragmentFragB { Arguments = new Bundle() }; ;
                    break;
                default:
                    fragment = HomeFragment.NewInstance();
                    break;
            }

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.content_frame, fragment)
                .Commit();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }



    }
}