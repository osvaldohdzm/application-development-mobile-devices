﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using System.Threading.Tasks;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.MyTheme")]
    public class ActivityImageButton : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_image_button);
        }

    }
}