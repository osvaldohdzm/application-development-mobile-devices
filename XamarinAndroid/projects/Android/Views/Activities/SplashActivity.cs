﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using System.Threading.Tasks;

namespace AsuraEducationPlatform.Droid.Views
{
    [Activity(Label = "@string/app_name", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            SimulateStartup();
        }

        // Prevent the back button from canceling the startup process
        public override void OnBackPressed() { }

        async void SimulateStartup()
        {
            await Task.Delay(0);
            StartActivity(new Intent(Application.Context, typeof(HomeActivity)));
        }
    }
}