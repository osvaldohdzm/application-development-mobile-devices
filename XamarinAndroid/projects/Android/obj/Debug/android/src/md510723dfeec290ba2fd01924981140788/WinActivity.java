package md510723dfeec290ba2fd01924981140788;


public class WinActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("AsuraEducationPlatform.Droid.Views.WinActivity, AsuraEducationPlatform", WinActivity.class, __md_methods);
	}


	public WinActivity ()
	{
		super ();
		if (getClass () == WinActivity.class)
			mono.android.TypeManager.Activate ("AsuraEducationPlatform.Droid.Views.WinActivity, AsuraEducationPlatform", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
