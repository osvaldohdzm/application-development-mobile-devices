package md510723dfeec290ba2fd01924981140788;


public class CourseViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("AsuraEducationPlatform.Droid.Views.CourseViewHolder, AsuraEducationPlatform", CourseViewHolder.class, __md_methods);
	}


	public CourseViewHolder (android.view.View p0)
	{
		super (p0);
		if (getClass () == CourseViewHolder.class)
			mono.android.TypeManager.Activate ("AsuraEducationPlatform.Droid.Views.CourseViewHolder, AsuraEducationPlatform", "Android.Views.View, Mono.Android", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
